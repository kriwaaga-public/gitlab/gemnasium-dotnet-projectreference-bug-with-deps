Simple reproduction of Dependency Scanner crash on .Net _packages.lock.json_ input.


Observes bug (see pipeline):
```
{
  "version": 1,
  "dependencies": {
    ".NETCoreApp,Version=v5.0": {
      "dep1": {
        "type": "Project",
        "dependencies": {
          "Dep2": "1.0.0"
        }
      },
      "dep2": {
        "type": "Project"
      }
    }
  }
```
Results in error:
```
/analyzer run
[INFO] [Gemnasium] [2021-11-10T12:49:37Z] ▶ GitLab Gemnasium analyzer v2.29.11
[INFO] [Gemnasium] [2021-11-10T12:49:38Z] ▶ Using commit fecf22e2c9d37a8c8621023f0c668f28dda06c11
 of vulnerability database
[FATA] [Gemnasium] [2021-11-10T12:49:38Z] ▶ cannot find nuget dependency: Dep2
```


Works without errors, directories and project names all lower-case:
```
{
  "version": 1,
  "dependencies": {
    ".NETCoreApp,Version=v5.0": {
      "dep1": {
        "type": "Project",
        "dependencies": {
          "dep2": "1.0.0"
        }
      },
      "dep2": {
        "type": "Project"
      }
    }
  }
```
